# Config

Config files that I use for applications.

This includes applications built with Arbinger, and then also applications that I build for myself. This repository can act as a very quick start for me to get this basic config up and going for all projects I'm working on.

Thus eliminating an hour or two of work that I generally do whenever I start a fresh project.
