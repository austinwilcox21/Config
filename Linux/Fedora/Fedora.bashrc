# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
alias ls='lsd'
alias lsa='lsd -all'

export PATH="$HOME/.emacs.d/bin:$PATH"
export PATH="/opt/mssql/bin:$PATH"
export DOTNET_ROOT="/opt/dotnet:$PATH"

neofetch
