sudo dnf update

sudo dnf install dnf-plugins-core 
sudo dnf config-manager --add-repo https://brave-browser-rpm-release.s3.brave.com/x86_64/

sudo rpm --import https://brave-browser-rpm-release.s3.brave.com/brave-core.asc

sudo dnf install brave-browser

sudo rpm --import https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg 
printf "[gitlab.com_paulcarroty_vscodium_repo]\nname=gitlab.com_paulcarroty_vscodium_repo\nbaseurl=https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/rpms/\nenabled=1\ngpgcheck=1\nrepo_gpgcheck=1\ngpgkey=https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg" |sudo tee -a /etc/yum.repos.d/vscodium.repo

sudo dnf install codium

sudo dnf install git nodejs npm

sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo wget -O /etc/yum.repos.d/microsoft-prod.repo https://packages.microsoft.com/config/fedora/33/prod.repo

sudo dnf install dotnet-sdk-5.0

# Azure Help
# https://docs.microsoft.com/en-us/azure/devops/repos/git/use-ssh-keys-to-authenticate?view=azure-devops
# This walks through adding the correct value to your ~/.ssh/config that will allow you to pull down from azure

sudo dnf install lsd emacs

git clone --depth 1 https://github.com/hlissner/doom-emacs ~/.emacs.d
~/.emacs.d/bin/doom install

sudo dnf install @virtualization
echo "Edit libvirtd config"
echo "sudo vi /etc/libvirt/libvirtd.conf was copied to your clipboard"
xclip -selection clipboard < "sudo vi /etc/libvrit/libvirtd.conf"

echo "Set doman socket to group ownership to libvirt"
echo "unix_sock_group = "libvirt""
echo "adjust unix socket permissions for the R/W Socket"
echo "unix_sock_rw_perms = "0770""

echo "Start libvirtd service"
echo "sudo systemctl start libvirtd"
echo "sudo systemctl enable libvirtd"

echo "sudo usermod -a -G libvirt $(whoami)"

sudo curl -o /etc/yum.repos.d/mssql-server.repo https://packages.microsoft.com/config/rhel/8/mssql-server-2019.repo
sudo yum install -y mssql-server

echo "To complete mssql server installation."
echo "sudo /opt/mssql/bin/mssql-conf setup"
echo "systemctl status mssql-server"
echo "To connect locally, sqlcmd -S localhost -U SA -P '<YourPassword>'"

sudo dnf install java-11-openjdk-devel
java -version

wget https://dbeaver.io/files/dbeaver-ce-latest-stable.x86_64.rpm ~/Downloads/

sudo dnf install neofetch

sudo dnf install gimp

