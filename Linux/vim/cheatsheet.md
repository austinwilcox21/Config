# Vim Cheatsheet


## Commands
Horizontal Split, opens same file
```
:split
```
or
```
:sp
```

Vertical Split
```
:vsplit
```
or
```
:vs
```

Close the split
```
:q
```
or
```
CTRL+w+c
```

Specify the height of a split
```
:10sp
```

Even out the height of all splits
```
CTRL+=
```

Make split as big as it can be horizontal 
```
CTRL+w+_
```

Open terminal in vim
```
:vs|:terminal
```
```
:tt
```




