const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const cssnano = require('gulp-cssnano');
const rename = require('gulp-rename');
const notify = require('gulp-notify');
const del = require('del');

function styles() {
 return gulp.src('src/index.css')
  .pipe(autoprefixer())
  .pipe(gulp.dest('src/CSS'))
  .pipe(rename({ suffix: '.min' }))
  .pipe(cssnano())
  .pipe(gulp.dest('src/CSS'))
  .pipe(notify({ message: 'Styles task complete' }))
}

gulp.task('styles', () => {return styles()});

gulp.task('watch:styles', function() {
  gulp.watch('src/index.css', function() {
    return styles();
  })
})
