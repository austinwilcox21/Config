npm install --save-dev del eslint eslint-config-airbnb eslint-plugin-import eslint-plugin-jsx-a11y eslint-plugin-react eslint-plugin-react-hooks gulp gulp-autoprefixer gulp-concat gulp-cssnano gulp-notify gulp-rename prettier

#Script in package.json for prettier
# "format": "prettier --write \"src/**/*.{js,jsx,ts,tsx,json,md}\""
xclip "format": "prettier --write \"src/**/*.{js,jsx,ts,tsx,json,md}\""
echo "Prettier script copied to clipboard";
